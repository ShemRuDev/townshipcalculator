﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    public class Product
    {
        public int MinLevel { get; set; }
        public int SellValue { get; set; }
        public int XP { get; set; }
        public int TimeToComplete { get; set; }
        public List<ProductType> Dependencies { get; set; }

        public Product()
        {
            Dependencies = new List<ProductType>();
        }
    }
}
