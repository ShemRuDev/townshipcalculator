﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    public enum ProductType
    {
        Wheat,
        Corn,
        Carrot,
        Sugarcane,
        Cotton,
        Strawberry,
        Tomato,
        Pine,

        Milk,
        Egg,
        Wool,

        Bread,
        Cookies,
        Bagel,

        Cream,
        Cheese,
        Butter,

        Sugar,
        Syrup,

        CottonFabric,
        Yarn,

        Shirt,
        Sweater,
        
        Popcorn,
        CornChips,
        Granola,

        Milkshake,
        Cheeseburger,
        Sandwich,

        Paper
    }
}
