﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    // Online Time - for Fast Products
    // Offline Time - for Slow
    public class Player
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int SessionsCount { get; set; }
        public int[] PlayingSchedule { get; set; }

        public Player() { }

        // For creating with Equal sessions
        public Player(int sessionsCount, int offlinePeriodMinutes)
        {
            SessionsCount = sessionsCount;

            if (offlinePeriodMinutes <= 0)
                offlinePeriodMinutes = 1;

            PlayingSchedule = new int[sessionsCount * 2 + 1];
            int totalOfflineTime = (sessionsCount - 1) * offlinePeriodMinutes;
            int sessionMinutes = (AppConstants.MaxMinutesOnline - totalOfflineTime) / sessionsCount;

            for (int i = 0; i < sessionsCount; i += 2)
            {
                PlayingSchedule[i] = sessionMinutes;
                PlayingSchedule[i + 1] = offlinePeriodMinutes;
            }

            // Sleepy Time ))
            PlayingSchedule[PlayingSchedule.Length - 1] = AppConstants.MunutesInDay - totalOfflineTime - sessionMinutes * sessionsCount;
        }
    }
}
