﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    // Minutes in Day = 1440 minutes (MAX LIMIT for OnlineAmount)
    class Program
    {
        private static readonly Dictionary<ProductType, Product> MainProducts = new Dictionary<ProductType, Product>()
        {
            { ProductType.Wheat, new Product() { MinLevel = 1, SellValue = 1, XP = 1, TimeToComplete = 2} },
            { ProductType.Corn, new Product() { MinLevel = 2, SellValue = 3, XP = 1, TimeToComplete = 5} },
            { ProductType.Carrot, new Product() { MinLevel = 4, SellValue = 5, XP = 2, TimeToComplete = 10} },
            { ProductType.Sugarcane, new Product() { MinLevel = 6, SellValue = 7, XP = 3, TimeToComplete = 20} },
            { ProductType.Cotton, new Product() { MinLevel = 9, SellValue = 9, XP = 4, TimeToComplete = 30} },
            { ProductType.Strawberry, new Product() { MinLevel = 13, SellValue = 11, XP = 5, TimeToComplete = 60} },
            { ProductType.Tomato, new Product() { MinLevel = 16, SellValue = 13, XP = 6, TimeToComplete = 120} },
            { ProductType.Pine, new Product() { MinLevel = 18, SellValue = 15, XP = 7, TimeToComplete = 180} },

            { ProductType.Milk, new Product() { MinLevel = 1, SellValue = 7, XP = 3, TimeToComplete = 20} },
            { ProductType.Egg, new Product() { MinLevel = 5, SellValue = 10, XP = 4, TimeToComplete = 60} },
            { ProductType.Wool, new Product() { MinLevel = 10, SellValue = 15, XP = 6, TimeToComplete = 240} },

            { ProductType.Bread, new Product() { MinLevel = 2, SellValue = 5, XP = 2, TimeToComplete = 5,
                Dependencies = new List<ProductType>() { ProductType.Wheat} } },
            { ProductType.Cookies, new Product() { MinLevel = 5, SellValue = 44, XP = 19, TimeToComplete = 15,
                Dependencies = new List<ProductType>() { ProductType.Wheat, ProductType.Egg } } },
            { ProductType.Bagel, new Product() { MinLevel = 8, SellValue = 55, XP = 24, TimeToComplete = 30,
                Dependencies = new List<ProductType>() { ProductType.Wheat, ProductType.Sugar, ProductType.Egg} } },

            { ProductType.Cream, new Product() { MinLevel = 4, SellValue = 12, XP = 5, TimeToComplete = 15,
                Dependencies = new List<ProductType>() { ProductType.Milk} } },
            { ProductType.Cheese, new Product() { MinLevel = 6, SellValue = 25, XP = 11, TimeToComplete = 30,
                Dependencies = new List<ProductType>() { ProductType.Milk} } },
            { ProductType.Butter, new Product() { MinLevel = 11, SellValue = 39, XP = 17, TimeToComplete = 60,
                Dependencies = new List<ProductType>() { ProductType.Milk} } },

            { ProductType.Sugar, new Product() { MinLevel = 7, SellValue = 14, XP = 6, TimeToComplete = 20,
                Dependencies = new List<ProductType>() { ProductType.Sugarcane} } },
            { ProductType.Syrup, new Product() { MinLevel = 17, SellValue = 29, XP = 12, TimeToComplete = 40,
                Dependencies = new List<ProductType>() { ProductType.Sugarcane} } },

            { ProductType.CottonFabric, new Product() { MinLevel = 9, SellValue = 37, XP = 16, TimeToComplete = 30,
                Dependencies = new List<ProductType>() { ProductType.Cotton} } },
            { ProductType.Yarn, new Product() { MinLevel = 10, SellValue = 61, XP = 26, TimeToComplete = 40,
                Dependencies = new List<ProductType>() { ProductType.Wool} } },

            { ProductType.Shirt, new Product() { MinLevel = 12, SellValue = 45, XP = 19, TimeToComplete = 60,
                Dependencies = new List<ProductType>() { ProductType.CottonFabric} } },
            { ProductType.Sweater, new Product() { MinLevel = 14, SellValue = 76, XP = 33, TimeToComplete = 90,
                Dependencies = new List<ProductType>() { ProductType.Yarn} } },

            { ProductType.Popcorn, new Product() { MinLevel = 14, SellValue = 12, XP = 5, TimeToComplete = 30,
                Dependencies = new List<ProductType>() { ProductType.Corn} } },
            { ProductType.CornChips, new Product() { MinLevel = 17, SellValue = 19, XP = 8, TimeToComplete = 60,
                Dependencies = new List<ProductType>() { ProductType.Corn} } },
            { ProductType.Granola, new Product() { MinLevel = 19, SellValue = 29, XP = 12, TimeToComplete = 40,
                Dependencies = new List<ProductType>() { ProductType.Wheat, ProductType.Strawberry} } },

            { ProductType.Milkshake, new Product() { MinLevel = 16, SellValue = 30, XP = 13, TimeToComplete = 15,
                Dependencies = new List<ProductType>() { ProductType.Milk, ProductType.Strawberry} } },
            { ProductType.Cheeseburger, new Product() { MinLevel = 16, SellValue = 57, XP = 24, TimeToComplete = 30,
                Dependencies = new List<ProductType>() { ProductType.Bread, ProductType.Cheese, ProductType.Tomato} } },
            { ProductType.Sandwich, new Product() { MinLevel = 19, SellValue = 80, XP = 34, TimeToComplete = 40,
                Dependencies = new List<ProductType>() { ProductType.Bread, ProductType.Butter, ProductType.Strawberry} } },

            { ProductType.Paper, new Product() { MinLevel = 18, SellValue = 19, XP = 8, TimeToComplete = 90,
                Dependencies = new List<ProductType>() { ProductType.Pine} } }
        };

        private static readonly List<Player> PreconfiguredPlayers = new List<Player>()
        {
            // Schedule in form Online - Offline
            new Player() { Name = "Anna", Level = 12, SessionsCount = 2, PlayingSchedule = new int[] { 10, 600, 15, 815 } }, 
            new Player() { Name = "Boris", Level = 15, SessionsCount = 4, PlayingSchedule = new int[] { 10, 180, 60, 180, 60, 180, 10, 760 } },
            new Player() { Name = "Vovan", Level = 19, PlayingSchedule = new int[] { 300, 60, 240, 30, 120, 690 } }
        };

        private static TotalStatistics TotalStats = new TotalStatistics();

        private const string LogFile = "stat.log";

        static void Main(string[] args)
        {
            bool isUseOrdersCancel = false;

            Console.WriteLine("***** Вас приветствует эмулятор Игрока в Township! *****");
            Console.WriteLine("Выберите персонаж:");
            Console.WriteLine("1 - Анна");
            Console.WriteLine("2 - Борис");
            Console.WriteLine("3 - Вован");
            Console.WriteLine("4 - Создать своего...");

            int ordersCount = AppConstants.DefaultOrdersCount;
            string ch = Console.ReadLine();
            Player currentPlayer = null;
            switch (ch)
            {
                case "1":
                    currentPlayer = PreconfiguredPlayers[0];
                    break;
                case "2":
                    currentPlayer = PreconfiguredPlayers[1];
                    break;
                case "3":
                    currentPlayer = PreconfiguredPlayers[2];
                    break;
                case "4":
                    // TODO:
                    // - Name
                    // - Level
                    // - Schedule
                    // - Orders NUM (check that > 4 and <= 10)
                    break;
                default:
                    Console.WriteLine("Ну и до свидания!");
                    Console.ReadKey();
                    return;
            }

            Console.WriteLine("Разрешить Отмену заказов? (y/n):");
            string ocAnswer = Console.ReadLine();
            if (ocAnswer.ToLower() == "y")
                isUseOrdersCancel = true;

            Console.WriteLine("Сколько дней эмулировать: ");
            string daysStr = Console.ReadLine();
            int daysToEmulate;
            if (!int.TryParse(daysStr, out daysToEmulate))
            {
                Console.WriteLine("Число бы ввести");
                Console.ReadKey();
                return;
            }
            if (daysToEmulate <= 0)
                daysToEmulate = 1;

            var productsPool = MainProducts.Where(p => p.Value.MinLevel <= currentPlayer.Level).Select(p => p.Key).ToArray();
            int rngLimit = productsPool.Length;

            // Заказы представлены тем продуктом, который Дольше всего Готовится
            var random = new Random();
            ProductType[] ordersList = new ProductType[ordersCount];
            for (int i = 0; i < ordersCount; i++)
            {
                ordersList[i] = productsPool[random.Next(rngLimit)];
            }

            AppendToFileLog(string.Format("*** Player \"{0}\" ***", currentPlayer.Name));
            //AppendToFileLog(string.Format("Schedule:"));

            // MAIN LOOPS
            for (int d = 0; d < daysToEmulate; d++)
            {
                Console.WriteLine("### День {0} ###", d + 1);

                for (int i = 0; i < currentPlayer.PlayingSchedule.Length; i++)
                {
                    int minutesAmount = currentPlayer.PlayingSchedule[i];

                    // Online Duration
                    if(i % 2 == 0)
                    {
                        Console.WriteLine("||| Игровая Сессия № {0}. Время Игры: {1} мин. |||", i/2 + 1, minutesAmount);
                        PrintOrders(ordersList);

                        bool isOnlineTimeSpent = false;
                        int remainedMinutes = minutesAmount;

                        // Replay until Remained Minutes == 0 OR No more Orders to afford Found
                        do
                        {
                            // Check what Orders we can Take
                            int longestOrder = -1;
                            for (int o = 0; o < ordersCount; o++)
                            {
                                // Yeah, here we can use Cache of cz
                                // But it would be better to take PureMinutes to Metadata
                                int timeToOrderComplete = GetPureMinutes(ordersList[o]);
                                if (timeToOrderComplete <= remainedMinutes)
                                {
                                    Console.WriteLine("Выполняем Заказ на [{0}]", ordersList[o]);
                                    CountTotalStatistics(ordersList[o]);

                                    // DO Order -> Change for new One
                                    ordersList[o] = productsPool[random.Next(rngLimit)];
                                    
                                    if (timeToOrderComplete > longestOrder)
                                        longestOrder = timeToOrderComplete;
                                }
                            }

                            // No Suitable Orders were found
                            if (longestOrder == -1)
                            {
                                Console.WriteLine("Не найдено выполнимых заказов");
                                Console.WriteLine();

                                if (isUseOrdersCancel)
                                {
                                    // Cancel all orders that requires time more than available
                                    for (int o = 0; o < ordersCount; o++)
                                    {
                                        int timeToOrderComplete = GetPureMinutes(ordersList[o]);
                                        if (timeToOrderComplete > remainedMinutes)
                                        {
                                            TotalStats.TotalCancelledOrdersCount++;
                                            Console.WriteLine("Отменен Заказ на [{0}]", ordersList[o]);

                                            // Change for new One
                                            ordersList[o] = productsPool[random.Next(rngLimit)];
                                        }
                                    }
                                    // Remained minutes - Time for new Order generation
                                    remainedMinutes -= AppConstants.NewOrderGenerationTime;
                                    if (remainedMinutes <= 0)
                                        isOnlineTimeSpent = true;

                                    PrintOrders(ordersList);
                                    Console.WriteLine("Осталось времени: [{0}] мин.", remainedMinutes);
                                }
                                else
                                    isOnlineTimeSpent = true;
                            }
                            else
                            {
                                // Only new Orders displayed

                                CountActiveTimeStat(longestOrder);
                                remainedMinutes -= longestOrder;

                                PrintOrders(ordersList);
                                Console.WriteLine("Осталось времени: [{0}] мин.", remainedMinutes);

                                if (remainedMinutes <= 0)
                                    isOnlineTimeSpent = true;
                            }
                        } while (!isOnlineTimeSpent);
                    }
                    // Offline
                    else
                    {
                        Console.WriteLine("< OFFLINE TIME FOR {0} Minutes >", minutesAmount);
                        //PrintOrders(ordersList);

                        // Searching longest Order. Thinking that dependency products available
                        int longestOrder = -1;
                        int selectedOrder = -1;
                        for (int o = 0; o < ordersCount; o++)
                        {
                            int timeToOrderComplete = GetPureMinutes(ordersList[o]);
                            if (timeToOrderComplete > longestOrder && timeToOrderComplete <= minutesAmount)
                            {
                                longestOrder = timeToOrderComplete;
                                selectedOrder = o;
                            }
                        }

                        if (longestOrder == -1)
                        {
                            Console.WriteLine("Ничего не успели выполнить");
                        }
                        else
                        {
                            var completedProduct = ordersList[selectedOrder];
                            ordersList[selectedOrder] = productsPool[random.Next(rngLimit)];    // Generate new Not when Online occurs!
                            Console.WriteLine("В Оффлайне выполнен Заказ на [{0}]", completedProduct);
                        }

                        Console.WriteLine();
                    }
                }

                Console.WriteLine("Результаты Дня:");
                Console.WriteLine("Времени онлайн потрачено: {0}; Получено опыта: {1}; Заработано в золоте*: {2}{3}",
                    TotalStats.TotalActiveTimeSpent, TotalStats.TotalXP, TotalStats.TotalGold, 
                    isUseOrdersCancel ? string.Format(" Отменено заказов: {0}", TotalStats.TotalCancelledOrdersCount) : "");
                AppendToFileLog(string.Format("Day {0}: Time: {1} XP: {2} Gold: {3} CancelledCnt: {4}", d + 1,
                    TotalStats.TotalActiveTimeSpent, TotalStats.TotalXP, TotalStats.TotalGold, TotalStats.TotalCancelledOrdersCount));
                Console.WriteLine("*(по ценам продажи из Амбара)");
                Console.WriteLine();
            }

            Console.ReadKey();
        }

        private static void PrintOrders(ProductType[] ordersList)
        {
            Console.WriteLine("Доступны заказы:");
            foreach (var pt in ordersList)
            {
                Console.Write("{0};", pt);
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        private static int GetPureMinutes(ProductType targetProduct)
        {
            int pureMinutes = 0;

            var currentProduct = MainProducts[targetProduct];
            pureMinutes += currentProduct.TimeToComplete;

            var dependenciesList = currentProduct.Dependencies;
            foreach (var pt in dependenciesList)
                pureMinutes += GetPureMinutes(pt);

            return pureMinutes;
        }

        private static int GetPureXP(ProductType targetProduct)
        {
            int pureXP = 0;

            var currentProduct = MainProducts[targetProduct];
            pureXP += currentProduct.XP;

            var dependenciesList = currentProduct.Dependencies;
            foreach (var pt in dependenciesList)
                pureXP += GetPureXP(pt);

            return pureXP;
        }

        private static void CountTotalStatistics(ProductType pt)
        {
            var product = MainProducts[pt];

            TotalStats.TotalGold += product.SellValue;
            TotalStats.TotalXP += GetPureXP(pt);

            //TotalStats.TotalGoldForOrder += (int)(product.SellValue * AppConstants.OrderKoeff);
        }

        private static void CountActiveTimeStat(int minutesSpent)
        {
            TotalStats.TotalActiveTimeSpent += minutesSpent;
        }

        private static void AppendToFileLog(string message)
        {
            File.AppendAllText(Path.Combine(Environment.CurrentDirectory, LogFile), string.Format("{0}{1}", message, Environment.NewLine));
        }
    }
}

