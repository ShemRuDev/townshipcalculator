﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    public class TotalStatistics
    {
        public int TotalActiveTimeSpent { get; set; }

        public int TotalGold { get; set; }
        public int TotalXP { get; set; }

        public int TotalCancelledOrdersCount { get; set; }

        //public int TotalGoldForOrder { get; set; }
    }
}
