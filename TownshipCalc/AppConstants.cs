﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownshipCalc
{
    public static class AppConstants
    {
        public const int MaxMinutesOnline = 1080;   // Maximum time that player can play
        public const int MunutesInDay = 1440;
        public const int DefaultOrdersCount = 6;
        public const int NewOrderGenerationTime = 5;
        public const float OrderKoeff = 1.2f;
    }
}
